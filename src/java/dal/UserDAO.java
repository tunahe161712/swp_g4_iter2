/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Role;
import model.User;

/**
 *
 * @author DELL
 */
public class UserDAO extends DBContext {

    public User login(String email, String password) { // day la kiem tra xem co so dien thoai trung ko: tuc la phone_number la khoa
        String sql = "select * from users where uemail = ? "
                + "and upwad = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, getMd5(password));
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Role r = getRoleById(rs.getInt("roleID"));
                    return new User(rs.getInt("id"), rs.getString("uname"), rs.getString("upwad"), rs.getString("uemail"),
                        rs.getString("umobile"),r);
            }
        } catch (Exception e) {
        }
        return null;
    }
    

    public Role getRoleById(int id) { // id này là của category id bên trong sản phầm products
        String sql = "select * from role where id = ?"; // chú ý sửa chỗ này

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);                           // chú ý sửa chỗ này
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Role r = new Role();
                // lay bang id cua product
                // lấy ra cái tên thương hiệu của sản phẩm đấy
                r.setName(rs.getString("name"));
                return r;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void insert(User u) {
        String sql = "insert into users(uname,upwad,uemail,umobile,roleID) values(?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getUname());
            st.setString(2, getMd5(u.getUpwad()));
            st.setString(3, u.getUemail());
            st.setString(4, u.getUmobile());
            
            st.setInt(5, u.getRoleID2());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public User login2(String email, String phone_number) { // day la kiem tra xem co so dien thoai trung ko: tuc la phone_number la khoa
        String sql = "select * from users where uemail = ? or umobile = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, phone_number);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
//               return new User(rs.getInt("userId"), rs.getString("fullname"),email , rs.getString("phone_number"), rs.getString("address"), rs.getString("password"), rs.getDate("created_at"), rs.getInt("roleId"),rs.getDate("updated_at"), rs.getInt("deleted"));
                return new User(rs.getString("uname"), rs.getString("upwad"), rs.getString("uemail"),
                        rs.getString("umobile"), rs.getInt("roleID"), rs.getString("description"), rs.getString("major"), rs.getString("mode"));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public User getUserById(String email) {
        String sql = "select * from users where uemail = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);                           // chú ý sửa chỗ này
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User u = new User();
                Role r = getRoleById(rs.getInt("roleID"));
                return new User(rs.getInt("id"), rs.getString("uname"), rs.getString("upwad"), rs.getString("uemail"),
                        rs.getString("umobile"), r, rs.getString("description"), rs.getString("major"), rs.getString("mode"));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public User getUsers(int xId) {
        User s = null;
        String xUname, xUpwad, xEmail, xUmobile;
        Role xRoleID;
        String xDescription;
        String xMajor, xMode;
        String sql = "select * from users where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, xId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                xUname = rs.getString("uname");
                xUpwad = rs.getString("upwad");
                xEmail = rs.getString("uemail");
                xUmobile = rs.getString("umobile");
                xRoleID = getRoleById(rs.getInt("roleID"));
                xDescription = rs.getString("description");
                xMajor = rs.getString("major");
                xMode = rs.getString("mode");
                s = new User(xId, xUname, xUpwad, xEmail, xUmobile, xRoleID, xDescription, xMajor, xMode);
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return (s);
    }

    public void update(int id, String uname, String uemail, String umobile, String description, String major, String mode) {
        String sql = "UPDATE users\n"
                + "SET uname = ?,\n"
                + "uemail = ?,\n"
                + "umobile = ?,\n"
                + "description = ?,\n"
                + "major = ?, \n"
                + "mode = ?\n"
                + " WHERE id =  " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, uname);
            st.setString(2, uemail);
            st.setString(3, umobile);
            st.setString(4, description);
            st.setString(5, major);
            st.setString(6, mode);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

  
    public String getMd5(String input) 
    { 
        try { 
            // Static getInstance method is called with hashing MD5 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
  
            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte 
            byte[] messageDigest = md.digest(input.getBytes()); 
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        // For specifying wrong message digest algorithms 
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    }
    
      public static void main(String[] args) {
        UserDAO u = new UserDAO();
        u.update(7, "abdddd", "abcd@gmail.com", "1234567899", "content here", "SE", "chính quy");
//        User user = u.getUsers(7);
//        System.out.println(user);
    }

}
