/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Curriculum;

/**
 *
 * @author Admin
 */
public class CurriculumDAO extends DBContext {

    public ArrayList<Curriculum> getCurriculumAdmin(String search, String typeSearch, int index, int size) {
        ArrayList<Curriculum> list = new ArrayList<Curriculum>();
        String sql = "";
        switch (typeSearch) {
            case "1":
                sql = "SELECT id,Name, Code, SUBSTRING(Description, 1, 100),DecisionNo, TotalCredit,active FROM curriculum\n"
                        + " where  Code like '%" + search + "%'\n"
                        + " order by id asc limit " + (index - 1) * size + "," + size * index + ";";
                break;
            case "2":
                sql = "SELECT id,Name, Code, SUBSTRING(Description, 1, 100),DecisionNo, TotalCredit,active FROM curriculum\n"
                        + " where Name like '%" + search + "%'\n"
                        + " order by id asc limit " + (index - 1) * size + "," + size * index + ";";
                break;
        }

        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getInt(6), rs.getBoolean(7)));
                
            }
        } catch (Exception e) {
        }
        return list;
    }
    public ArrayList<Curriculum> getCurriculum(String search, String typeSearch, int index, int size) {
        ArrayList<Curriculum> list = new ArrayList<Curriculum>();
        String sql = "";
        switch (typeSearch) {
            case "1":
                sql = "SELECT id,Name, Code, SUBSTRING(Description, 1, 100),DecisionNo, TotalCredit,active FROM curriculum\n"
                        + " where active = true and Code like '%" + search + "%'\n"
                        + " order by id asc limit " + (index - 1) * size + "," + size * index + ";";
                break;
            case "2":
                sql = "SELECT id,Name, Code, SUBSTRING(Description, 1, 100),DecisionNo, TotalCredit,active FROM curriculum\n"
                        + " where  active = true Name like '%" + search + "%'\n"
                        + " order by id asc limit " + (index - 1) * size + "," + size * index + ";";
                break;
        }

        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getInt(6), rs.getBoolean(7)));
                
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public Curriculum getCurriculum(String id) {
        String sql = "";
        sql = "SELECT * FROM curriculum\n"
                + " where id = '" + id + "'";
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getInt(6), rs.getBoolean(7));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int countTotalCurriculum(String search, String typeSearch) {
        int count = 0;
        String sql = "";
        switch (typeSearch) {
            case "1":
                sql = "SELECT count(*) FROM curriculum\n"
                        + " where Code like '%" + search + "%'\n"
                        + " order by id asc  ;";
                break;
            case "2":
                sql = "SELECT count(*) FROM curriculum\n"
                        + " where Name like '%" + search + "%'\n"
                        + " order by id asc  ;";
                break;
        }
      
        try {
            System.out.println(sql);
              PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            System.out.println("ok");
            while (rs.next()) {
                System.out.println(rs.getInt(1));
                count = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(new CurriculumDAO().getCurriculum( "1").getCode());
    }

    public void deleteCurriculum(String id) {
        String sql_update = "DELETE FROM  curriculum WHERE id = " + id;
        try {
            PreparedStatement stm = connection.prepareStatement(sql_update);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void changeStatusCurriculum(String id, String status) {
        String sql_update = "UPDATE `swp`.`curriculum` SET `active` = b'"+status+"' WHERE (`id` = '"+id+"')";
        try {
            PreparedStatement stm = connection.prepareStatement(sql_update);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void addCurriculum(String name, String code, String desc, String decision, String credit) {
        String sql = "INSERT INTO `swp`.`curriculum` (`Name`, `Code`, `Description`, `DecisionNo`, `TotalCredit`) VALUES ('" + name + "', '" + code + "', '" + desc + "', '" + decision + "', '" + credit + "');";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editCurriculum(String name, String code, String desc, String decision, String credit, String id) {
        String sql = "UPDATE `swp`.`curriculum` \n"
                + " SET `Name` = '" + name + "', `Code` = '" + code + "', `Description` = ?, `DecisionNo` = '" + decision + "', `TotalCredit` = '" + credit + "'\n"
                + " WHERE (`id` = '" + id + "');";
        try {
            System.out.println(sql);
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, desc);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}

