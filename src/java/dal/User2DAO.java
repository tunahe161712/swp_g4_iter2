package dal;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import model.User2;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author HP
 */
public class User2DAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<User2> getListUser() {
        ArrayList<User2> list = new ArrayList<>();
        String query = "select * from users2";
        try {
            conn = new DBContext().connection;//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new User2(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7)));
            }
            return list;
        } catch (Exception e) {
        }
        return null;
    }

    public void updateAccount(int id, String name, String email, String number) {
        String query = "update users2 \n"
                + "set uname =?, \n"
                + "uemail = ?,\n"
                + "umobile = ?\n"
                + "where id = ?";
        try {
            conn = new DBContext().connection;//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(4, id);
            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, number);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addUser(String name, String email, String password, String number, String role) {
        String query = "INSERT INTO swp.`users2` (uName, upwad, uemail, umobile, roleid) VALUES (?, ?, ?, ?, ?);";
        String enrText = "";
        try {
            MessageDigest msd = MessageDigest.getInstance("MD5");
            byte[] srcTextBytes = password.getBytes("UTF-8");
            byte[] enText = msd.digest(srcTextBytes);
            BigInteger big = new BigInteger(1, enText);
            enrText = big.toString(16);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User2DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(User2DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn = new DBContext().connection;//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ps.setString(2, enrText);
            ps.setString(3, email);
            ps.setString(4, number);
            ps.setInt(5, 2);

            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void lockUser(int id) {
        String query = "update users2 \n"
                + "set lockUser = 1\n"
                + "where id = ?";
        try {
            conn = new DBContext().connection;//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        User2DAO dao = new User2DAO();
        dao.lockUser(3);
    }
}
