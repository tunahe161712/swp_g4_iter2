/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.MaterialDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Material;
import model.Subject;

/**
 *
 * @author DELL
 */
@WebServlet(name = "MaterialUpdateServlet", urlPatterns = {"/updateMaterial"})
public class MaterialUpdateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MaterialUpdateServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MaterialUpdateServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    int id;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id");

        MaterialDAO md = new MaterialDAO();
        try {
            id = Integer.parseInt(id_raw);
            Material m = md.getByIDMaterial(id);

            List<Subject> list = md.getAllSubject("");
            request.setAttribute("dataSubject", list);
            request.setAttribute("material", m);

            request.getRequestDispatcher("MaterialUpdate.jsp").forward(request, response);
        } catch (Exception e) {
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mdes = request.getParameter("mdescription");
        String author = request.getParameter("author");
        String publisher = request.getParameter("publisher");
        String pDate = request.getParameter("pDate");
        String edition = request.getParameter("edition");

        String isbn = request.getParameter("isbn");
        String isMain_raw = request.getParameter("isMain");
        String isHCopy_raw = request.getParameter("isHCopy");
        String isOnline_raw = request.getParameter("isOnline");
        String note = request.getParameter("note");
        String sjCode = request.getParameter("sjCode");
        String status_raw = request.getParameter("status");
        PrintWriter out = response.getWriter();
        try {
            MaterialDAO md = new MaterialDAO();
            int isMain = Integer.parseInt(isMain_raw);
            int isHCopy = Integer.parseInt(isHCopy_raw);
            int isOnline = Integer.parseInt(isOnline_raw);
            int status = Integer.parseInt(status_raw);
            Subject s = md.getSubject(sjCode);
                Material m1 = md.getByIDMaterial(id);
            Material m = new Material(id, mdes, author, publisher, pDate, edition, isbn, isMain, isHCopy, isOnline, note, s, status);
           request.setAttribute("material", m);
            if (!md.isLengthValid(mdes, 20, 200)) { // nếu không nằm trong khoảng này thì cho quay về kia
                request.setAttribute("errorMdes", "The length of material description is must be larger 20 character");
            List<Subject> list = md.getAllSubject("");
            request.setAttribute("dataSubject", list);
//            request.setAttribute("material", m1);
            request.getRequestDispatcher("MaterialUpdate.jsp").forward(request, response);
            }
            else if ( author != null && !md.isLengthValid(author, 3, 50)) { // nếu không nằm trong khoảng này thì cho quay về kia
                request.setAttribute("errorAuthor", "The length of Author is must be larger 3 character");
               List<Subject> list = md.getAllSubject("");
            request.setAttribute("dataSubject", list);
//            request.setAttribute("material", m1);
            request.getRequestDispatcher("MaterialUpdate.jsp").forward(request, response);
            }
            else if (note != null && !md.isLengthValid(author, 3, 50)) { // nếu không nằm trong khoảng này thì cho quay về kia
                request.setAttribute("errorNote", "The length of Note is must be larger 3 character");
              List<Subject> list = md.getAllSubject("");
            request.setAttribute("dataSubject", list);
//            request.setAttribute("material", m1);
            request.getRequestDispatcher("MaterialUpdate.jsp").forward(request, response);
            }
            else if (pDate != null && !md.isYearValid(Integer.parseInt(pDate))) {
                request.setAttribute("errorYear", "Year is must be smaller the present year");
               List<Subject> list = md.getAllSubject("");
            request.setAttribute("dataSubject", list);
//            request.setAttribute("material", m1);
            request.getRequestDispatcher("MaterialUpdate.jsp").forward(request, response);
            } else {
                md.update(m);
                HttpSession session = request.getSession();
                session.removeAttribute("UpdateSuccess");
                session.setAttribute("UpdateSuccess", "success");
                response.sendRedirect("list");
            }
//out.print(s.toString());

        } catch (Exception e) {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
