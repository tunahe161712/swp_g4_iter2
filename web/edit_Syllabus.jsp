<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <title>FPT Learning Materials System</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">

        <!-- Additional CSS Files -->
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/templatemo-onix-digital.css">
        <link rel="stylesheet" href="assets/css/animated.css">
        <link rel="stylesheet" href="assets/css/owl.css">
        <link rel="stylesheet" href="assets/css/scroll_to_top.css">
        <link rel="stylesheet" href="css/style_banner.css">

        <style>
/*            .syllabus_detail{
                background: linear-gradient(45deg, #49a09d, #5f2c82);
            }*/

            #heading{
                font-size: 30px;
                color: black;
                text-transform: uppercase;
                font-weight: bold;
                text-align: center;
                margin-bottom: 15px;
            }
/*            table{
                width:100%;

                 for custom scrollbar for webkit browser

                ::-webkit-scrollbar {
                    width: 6px;
                }
                ::-webkit-scrollbar-track {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                }
                ::-webkit-scrollbar-thumb {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                }
            }

            td{
                color: white;
            }*/

            /*css button*/
            .btn-52,
            .btn-52 *,
            .btn-52 :after,
            .btn-52 :before,
            .btn-52:after,
            .btn-52:before {
                border: 0 solid;
                box-sizing: border-box;
            }
            .btn-52 {
                -webkit-tap-highlight-color: transparent;
                -webkit-appearance: button;
                background-color: #90d49f;
                background-image: none;
                color: #fff;
                cursor: pointer;
                font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont,
                    Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif,
                    Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
                font-size: 100%;
                line-height: 1.5;
                margin: 0;
                -webkit-mask-image: -webkit-radial-gradient(#000, #fff);
                padding: 0;
            }
            .btn-52:disabled {
                cursor: default;
            }
            .btn-52:-moz-focusring {
                outline: auto;
            }
            .btn-52 svg {
                display: block;
                vertical-align: middle;
            }
            .btn-52 [hidden] {
                display: none;
            }
            .btn-52 {
                border: 1px solid;
                border-radius: 999px;
                box-sizing: border-box;
                display: block;
                font-weight: 900;
                overflow: hidden;
                padding: 1.2rem 3rem;
                position: relative;
                text-transform: uppercase;
            }
            .btn-52 .original {
                background: #82c4c4;
                color: #fff;
                display: grid;
                inset: 0;
                place-content: center;
                position: absolute;
                transition: transform 0.2s;
            }
            .btn-52:hover .original {
                transform: translateY(100%);
            }
            .btn-52 .letters {
                display: inline-flex;
            }
            .btn-52 span {
                opacity: 0;
                transform: translateY(-15px);
                transition: transform 0.2s, opacity 0.2s;
            }
            .btn-52:hover span {
                opacity: 1;
                transform: translateY(0);
            }
            .btn-52:hover span:nth-child(2) {
                transition-delay: 0.1s;
            }
            .btn-52:hover span:nth-child(3) {
                transition-delay: 0.2s;
            }
            .btn-52:hover span:nth-child(4) {
                transition-delay: 0.3s;
            }
            .btn-52:hover span:nth-child(5) {
                transition-delay: 0.4s;
            }
            .btn-52:hover span:nth-child(6) {
                transition-delay: 0.5s;
            }


        </style>

    </head>
    <body>
        <!-- ***** Header Area Start ***** -->
        <jsp:include page="header1.jsp"/>
        <div class="main-image">
            <div class="heading_banner container">
                <h1><span>Syllabus</span></h1>
                <a class="button banner_button" href="#lists">scroll down to see website content &nbsp;<i class="fa fa-hand-point-down"></i></a>
            </div>
        </div>
        <!-- ***** Header Area End ***** -->

        <!--syllabus detail section start-->
        <div style="margin-top: 4rem;">
            <h1 id="heading" style="margin-bottom: 2rem;">Syllabus Details</h1>
            <div style="display: flex; justify-content: center">
                <form method="post" action="EditSyllabus" style="padding: 5px; width: 50%">
                    <label style="font-weight: bold">Syllabus ID: </label>
                    <input type="text" value="${s.sID}" name="sID" class="form-control">

                    <label style="font-weight: bold">Syllabus Name:</label>
                    <input type="text" name="sName" value="${s.sName}" class="form-control">

                    <label style="font-weight: bold">Syllabus English: </label>
                    <input type="text" name="sEnglish" value="${s.sEnglish}" class="form-control">

                    <label style="font-weight: bold">Subject Code: </label>
                    <input type="text" name="subjectCode" value="${s.subjectCode}" class="form-control">

                    <label style="font-weight: bold">NoCredit:</label>
                    <input type="text" name="noCredit" value="${s.noCredit}" class="form-control">

                    <label style="font-weight: bold">Degree Level: </label>
                    <input type="text" name="degreeLevel" value="${s.degreeLevel}" class="form-control">

                    <label style="font-weight: bold">Time Allocation: </label>
                    <textarea style="height: 250px" type="text" name="timeAllocation" class="form-control">${s.timeAllocation}</textarea>

                    <label style="font-weight: bold">Pre-Requisite: </label>
                    <textarea style="height: 250px" type="text" name="preRequisite" class="form-control">${s.preRequisite}</textarea>

                    <label style="font-weight: bold">Description:</label>
                    <textarea style="height: 250px"  name="description" class="form-control">${s.description}</textarea>

                    <label style="font-weight: bold">Student Task: </label>
                    <textarea style="height: 250px"  name="studentTask" class="form-control">${s.studentTask}</textarea>

                    <label style="font-weight: bold">Tools:</label>
                    <textarea style="height: 250px"  name="tools" class="form-control">${s.tools}</textarea>

                    <label style="font-weight: bold">Scoring Scale:</label>
                    <input type="text" name="scoringScale" value="${s.scoringScale}" class="form-control">

                    <label style="font-weight: bold">DecisionNo MM/dd/yyyy: </label>
                    <input type="text" name="decisionNo" value="${s.decisionNo}" class="form-control">

                    <label style="font-weight: bold">Is Approved:</label>
                    <input type="text" name="isApproved" value="${s.isApproved}" class="form-control">

                    <label style="font-weight: bold">Note: </label>
                    <textarea style="height: 250px"  name="note" class="form-control">${s.note}</textarea>

                    <label style="font-weight: bold">MinAvgMarkToPass:</label>
                    <input type="text" name="minAvgMarkToPass" value="${s.minAvgMarkToPass}" class="form-control">

                    <label style="font-weight: bold">IsActive:</label>
                    <input type="text" name="isActive" value="${s.isActive}" class="form-control">

                    <label style="font-weight: bold">Approved Date:</label>
                    <input type="text" name="approvedDate" value="${s.approvedDate}" class="form-control">

                    <div class="d-flex justify-content-around" style="margin-top: 2rem;">
                        <a href="viewSyllabusList#lists">
                            <button class="btn-52">
                                <div class="original">Return List</div>
                                <div class="letters">
                                    <span>R</span>
                                    <span>E</span>
                                    <span>T</span>
                                    <span>U</span>
                                    <span>R</span>
                                    <span>N</span>
                                </div>
                            </button>
                        </a>

                        <a href="EditSyllabus">
                            <button class="btn-52" type="submit">
                                <div class="original">Update</div>
                                <div class="letters">
                                    <span>U</span>
                                    <span>P</span>
                                    <span>D</span>
                                    <span>A</span>
                                    <span>T</span>
                                    <span>E</span>
                                </div>
                            </button>
                        </a>
                    </div>
                    <!--            <form action="EditSyllabus" method="post">
                                    <div class="syllabus_detail container">    
                                        <table class="table table-striped">
                                            <tr>
                                                <td>Syllabus ID: </td>
                                                <td>
                                                    <input type="text" value="${s.sID}" name="sID">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Syllabus Name: </td>
                                                <td>
                                                    <input type="text" name="sName" value="${s.sName}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Syllabus English: </td>
                                                <td>
                                                    <input type="text" name="sEnglish" value="${s.sEnglish}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Subject Code: </td>
                                                <td>
                                                    <input type="text" name="subjectCode" value="${s.subjectCode}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>NoCredit: </td>
                                                <td>
                                                    <input type="text" name="noCredit" value="${s.noCredit}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Degree Level: </td>
                                                <td>
                                                    <input type="text" name="degreeLevel" value="${s.degreeLevel}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Time Allocation: </td>
                                                <td>
                                                    <input type="text" name="timeAllocation" value="${s.timeAllocation}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pre-Requisite: </td>
                                                <td>
                                                    <input type="text" name="preRequisite" value="${s.preRequisite}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Description: </td>
                                                <td>
                                                    <input type="text" name="description" value="${s.description}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>StudentTasks: </td>
                                                <td>
                                                    <input type="text" name="studentTask" value="${s.studentTask}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tools: </td>
                                                <td>
                                                    <input type="text" name="tools" value="${s.tools}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scoring Scale: </td>
                                                <td>
                                                    <input type="text" name="scoringScale" value="${s.scoringScale}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>DecisionNo MM/dd/yyyy:</td>
                                                <td>
                                                    <input type="text" name="decisionNo" value="${s.decisionNo}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Is Approved: </td>
                                                <td>
                                                    <input type="text" name="isApproved" value="${s.isApproved}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Note: </td>
                                                <td>
                                                    <input type="text" name="note" value="${s.note}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>MinAvgMarkToPass: </td>
                                                <td>
                                                    <input type="text" name="minAvgMarkToPass" value="${s.minAvgMarkToPass}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>IsActive: </td>
                                                <td>
                                                    <input type="text" name="isActive" value="${s.isActive}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>ApprovedDate: </td>
                                                <td>
                                                    <input type="text" name="approvedDate" value="${s.approvedDate}">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                                
                                    button
                                    <div class="d-flex justify-content-around" style="margin-top: 2rem;">
                                        <a href="viewSyllabusList#lists">
                                            <button class="btn-52">
                                                <div class="original">Return List</div>
                                                <div class="letters">
                                                     Place each letter in its own element 
                                                    <span>R</span>
                                                    <span>E</span>
                                                    <span>T</span>
                                                    <span>U</span>
                                                    <span>R</span>
                                                    <span>N</span>
                                                </div>
                                            </button>
                                        </a>
                    
                                        <a href="EditSyllabus">
                                            <button class="btn-52" type="submit">
                                                <div class="original">Update</div>
                                                <div class="letters">
                                                     Place each letter in its own element 
                                                    <span>U</span>
                                                    <span>P</span>
                                                    <span>D</span>
                                                    <span>A</span>
                                                    <span>T</span>
                                                    <span>E</span>
                                                </div>
                                            </button>
                                        </a>
                                    </div>
                                </form>-->
            </div>


            <!--syllabus detail section end-->

            <!--footer section start-->
            <jsp:include page="footer1.jsp"/>
            <!--footer section end-->
    </body>
</html>
