
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="css/style_banner.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/templatemo-onix-digital.css">
        <link rel="stylesheet" href="assets/css/animated.css">
        <link rel="stylesheet" href="assets/css/owl.css">
        <link rel="stylesheet" href="assets/css/scroll_to_top.css">

    </head>
    <body>
        <jsp:include page="header1.jsp"/>
        <div style="margin-top: 50px ">
            <h1 style="text-align: center; margin-top: 10rem ">Add New Material</h1>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div style="display: flex; justify-content: center">
                        <form action="add" method ="post" style="padding: 5px; width: 50%">
                            <label style="font-weight: bold">Material Description *****:</label>
                            <c:set var = "c" value = "${requestScope.material}"/>
                            <textarea style="height: 250px" required name="mdescription" class="form-control">${c.material_description}</textarea>
                              <c:if test="${requestScope.errorMdes != null}">
                                <div style="color: red; text-align: center; font-size: bolder">${requestScope.errorMdes}</div>

                            </c:if>
                            <label style="font-weight: bold">Author:</label>
                            <input type="text" name="author" value="${c.author}" class="form-control">
                          
                                
                                   <c:if test="${requestScope.errorAuthor != null}">
                                <div style="color: red; text-align: center; font-size: bolder">${requestScope.errorAuthor}</div>

                            </c:if>
                            <label style="font-weight: bold">Publisher :</label>
                            <input type="text" name="publisher" value="${c.publisher}" class="form-control">
                            <label style="font-weight: bold">Published Date:</label>
                            <input type="text" name="pDate" value="${c.published_date}" class="form-control">
                            
                            <c:if test="${requestScope.errorYear != null}">
                                <div style="color: red; text-align: center; font-size: bolder">${requestScope.errorYear}</div>

                            </c:if>
                            
                            <label style="font-weight: bold">Edition:</label>
                            <input type="text" name="edition" value="${c.edition}" class="form-control">
                            <label style="font-weight: bold">ISBN:</label>
                            <input type="text" name="isbn" value="${c.isbn}" class="form-control">


                            <label style="font-weight: bold">Is Main true:</label>
                            <!--                            True      <input type="radio" id="id" name="isMain" value="1">
                                       false <input type="radio" id="id" name="isMain" value="0" ><br>-->

                            <select class="form-control" name="isMain">
                                <option value="1"> True </option>
                                <option value="0"> false </option>
                            </select>

                            <label style="font-weight: bold">Is Hard Copy:</label>

                            <select class="form-control" name="isHCopy">
                                <option value="1"> True </option>
                                <option value="0"> false </option>
                            </select>

                            <label style="font-weight: bold">Is Online :</label>


                            <select class="form-control" name="isOnline">
                                <option value="1"> True </option>
                                <option value="0"> false </option>
                            </select>

                            <label style="font-weight: bold">Note:</label>
                            <textarea style="height: 250px"  name="note" class="form-control">${c.note}</textarea>
                            <c:if test="${requestScope.errorNote != null}">
                                <div style="color: red; text-align: center; font-size: bolder">${requestScope.errorNote}</div>

                            </c:if>

                            <label style="font-weight: bold">Subject Code *****:</label>
                            <select name = "sjCode" class="form-control" required>
                                <c:forEach items="${requestScope.dataSubject}" var = "d">
                                    <option value="${d.subject_Code}">${d.subject_Code}</option>
                                </c:forEach>
                            </select>

                            <label style="font-weight: bold">Status :</label>

                            <select class="form-control" name="status">
                                <option value="1"> True </option>
                                <option value="0"> false </option>
                            </select>



                            <button style="margin: 10px" type="submit"  class="btn btn-primary">Add </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </body>

    <footer>
        <jsp:include page="footer1.jsp"/>
    </footer>
</html>

<script src="js/jquery.min.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/owl-carousel.js"></script>
<script src="assets/js/animation.js"></script>
<script src="assets/js/imagesloaded.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/js/scroll_to_top.js"></script>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/owl-carousel.js"></script>
<script src="assets/js/animation.js"></script>
<script src="assets/js/imagesloaded.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/js/scroll_to_top.js"></script>