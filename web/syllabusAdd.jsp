<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <title>FPT Learning Materials System</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">

        <!-- Additional CSS Files -->
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/templatemo-onix-digital.css">
        <link rel="stylesheet" href="assets/css/animated.css">
        <link rel="stylesheet" href="assets/css/owl.css">
        <link rel="stylesheet" href="assets/css/scroll_to_top.css">
        <link rel="stylesheet" href="css/style_banner.css">

        <style>
            .login-box {
                padding: 40px;
                background: rgba(0,0,0,.5);
                box-sizing: border-box;
                box-shadow: 0 15px 25px rgba(0,0,0,.6);
                border-radius: 10px;
            }

            .login-box h2 {
                margin: 0 0 30px;
                padding: 0;
                color: #fff;
                text-align: center;
            }

            .formbold-input-flex {
                gap: 20px;
                margin-bottom: 22px;
            }
            .formbold-input-flex > div {
                width: 100%;
                display: flex;
                flex-direction: column-reverse;
            }
            .formbold-textarea {
                display: flex;
                flex-direction: column-reverse;
            }

            .formbold-form-input {
                width: 100%;
                padding-bottom: 10px;
                border: none;
                border-bottom: 1px solid #DDE3EC;
                background: #FFFFFF;
                font-weight: 500;
                font-size: 16px;
                color: #07074D;
                outline: none;
                resize: none;
                border-radius:6px;
            }
            .formbold-form-input::placeholder {
                color: #536387;
            }
            .formbold-form-input:focus {
                border-color: #6A64F1;
            }
            .formbold-form-label {
                color: #07074D;
                font-weight: 500;
                font-size: 14px;
                line-height: 24px;
                display: block;
                margin-bottom: 18px;

            }
            .formbold-form-input:focus + .formbold-form-label {
                color: #6A64F1;
            }

            .formbold-btn {
                font-size: 16px;
                border-radius: 5px;
                padding: 12px 25px;
                border: none;
                font-weight: 500;
                background-color: #6A64F1;
                color: white;
                cursor: pointer;
                margin-top: 25px;
            }
            .formbold-btn:hover {
                box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.05);
            }

        </style>

    </head>
    <body>
        <!-- ***** Header Area Start ***** -->
        <jsp:include page="header1.jsp"/>
        <div class="main-image">
            <div class="heading_banner container">
                <h1><span>Syllabus</span></h1>
                <a class="button banner_button" href="#lists">scroll down to see website content &nbsp;<i class="fa fa-hand-point-down"></i></a>
            </div>
        </div>
        <!-- ***** Header Area End ***** -->

        <!--form add syllabus start-->
        <div class="container" style="margin-top: 5rem;">
            <div class="login-box">
                <h2>Create Syllabus</h2>
                <form action="addSyllabus" method="post">
                    <div class="formbold-input-flex">
                        <div>
                            <input type="text" name="SyllabusID" placeholder="" class="formbold-form-input" />
                            <label class="formbold-form-label"> Syllabus ID </label>
                        </div>
                    </div>
                    <div class="formbold-input-flex">
                        <div>
                            <input type="text" name="SubjectCode" placeholder="" class="formbold-form-input" />
                            <label class="formbold-form-label"> Subject Code </label>
                        </div>
                    </div>
                    
                    <div class="formbold-textarea">
                        <textarea rows="2" name="SyllabusName" placeholder="" class="formbold-form-input"></textarea>
                        <label class="formbold-form-label"> Syllabus Name </label>
                    </div>

                    <div class="formbold-input-flex">
                        <div>
                            <input type="text" name="isActive" placeholder="" class="formbold-form-input" />
                            <label class="formbold-form-label"> is Active (Active/Deactive) </label>
                        </div>
                    </div>
                    <div class="formbold-input-flex">
                        <div>
                            <input type="text" name="isApproved" placeholder="" class="formbold-form-input" />
                            <label class="formbold-form-label"> is Approved (Approved/Depproved)</label>
                        </div>
                    </div>

                    <div class="formbold-textarea">
                        <textarea rows="2" name="DecisionNo" placeholder="" class="formbold-form-input"></textarea>
                        <label class="formbold-form-label"> DecisionNo MM/dd/yyyy </label>
                    </div>
                   
                    <button class="formbold-btn" type="submit">
                        Add Syllabus
                    </button>
                </form>
            </div>
        </div>
        <!--form add syllabus end-->

        <!--footer section start-->
        <jsp:include page="footer1.jsp"/>
        <!--footer section end-->
    </body>
</html>
