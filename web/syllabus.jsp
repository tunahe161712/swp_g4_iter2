<%-- 
    Document   : syllabus
    Created on : Feb 16, 2023, 3:29:47 PM
    Author     : 84379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <title>FPT Learning Materials System</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">

        <!-- Additional CSS Files -->
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/templatemo-onix-digital.css">
        <link rel="stylesheet" href="assets/css/animated.css">
        <link rel="stylesheet" href="assets/css/owl.css">
        <link rel="stylesheet" href="assets/css/scroll_to_top.css">
        <link rel="stylesheet" href="css/style_banner.css">

        <style>
            .table-users{
                border-radius: 10px;
                border: 1px solid #fff;
                box-shadow: 10px 10px 0 rgba(0,0,0,0.1);
                overflow: hidden;
            }

            .header {
                background-color: #398B93;
                color: white;
                font-size: 1.5em;
                padding: 1rem;
                text-align: center;
                text-transform: uppercase;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                overflow: hidden;
                box-shadow: 0 0 20px rgba(0,0,0,0.1);
                background: linear-gradient(45deg, #49a09d, #5f2c82);
            }

            th,
            td {
                padding: 15px;
                background-color: rgba(255,255,255,0.2);
                color: #fff;
            }

            th {
                text-align: left;
            }

            thead {
                th {
                    background-color: #55608f;
                }
            }

            tbody {
                tr {
                    &:hover {
                        background-color: rgba(255,255,255,0.3);
                    }
                }
                td {
                    position: relative;
                    &:hover {
                        &:before {
                            content: "";
                            position: absolute;
                            left: 0;
                            right: 0;
                            top: -9999px;
                            bottom: -9999px;
                            background-color: rgba(255,255,255,0.2);
                            z-index: -1;
                        }
                    }
                }
            }

            a:hover{
                border-bottom: 1px double white;
            }

            .btn-18,
            .btn-18 *,
            .btn-18 :after,
            .btn-18 :before,
            .btn-18:after,
            .btn-18:before {
                border: 0 solid;
                box-sizing: border-box;
            }
            .btn-18 {
                -webkit-tap-highlight-color: transparent;
                -webkit-appearance: button;
                background-color: #90d49f;
                background-image: none;
                color: #fff;
                cursor: pointer;
                font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont,
                    Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif,
                    Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
                font-size: 100%;
                font-weight: 900;
                line-height: 1.5;
                margin: 0;
                -webkit-mask-image: -webkit-radial-gradient(#000, #fff);
                padding: 0;
                text-transform: uppercase;
            }
            .btn-18:disabled {
                cursor: default;
            }
            .btn-18:-moz-focusring {
                outline: auto;
            }
            .btn-18 svg {
                display: block;
                vertical-align: middle;
            }
            .btn-18 [hidden] {
                display: none;
            }
            .btn-18 {
                border-radius: 99rem;
                border-width: 2px;
                padding: 0.8rem 3rem;
                z-index: 0;
            }
            .btn-18,
            .btn-18 .text-container {
                overflow: hidden;
                position: relative;
            }
            .btn-18 .text-container {
                display: block;
            }
            .btn-18 .text {
                display: block;
                mix-blend-mode: difference;
                position: relative;
            }
            .btn-18:hover .text {
                -webkit-animation: move-up-alternate 0.3s forwards;
                animation: move-up-alternate 0.3s forwards;
            }
            @-webkit-keyframes move-up-alternate {
                0% {
                    transform: translateY(0);
                }
                50% {
                    transform: translateY(80%);
                }
                51% {
                    transform: translateY(-80%);
                }
                to {
                    transform: translateY(0);
                }
            }
            @keyframes move-up-alternate {
                0% {
                    transform: translateY(0);
                }
                50% {
                    transform: translateY(80%);
                }
                51% {
                    transform: translateY(-80%);
                }
                to {
                    transform: translateY(0);
                }
            }
            .btn-18:after,
            .btn-18:before {
                --tilt: 20px;
                background: #9ceef0;
                -webkit-clip-path: polygon(
                    0 0,
                    calc(100% - var(--tilt)) 0,
                    100% 50%,
                    calc(100% - var(--tilt)) 100%,
                    0 100%
                    );
                clip-path: polygon(
                    0 0,
                    calc(100% - var(--tilt)) 0,
                    100% 50%,
                    calc(100% - var(--tilt)) 100%,
                    0 100%
                    );
                content: "";
                height: 100%;
                left: -100%;
                position: absolute;
                top: 0;
                transition: transform 0.6s;
                width: 100%;
            }
            .btn-18:after {
                -webkit-clip-path: polygon(
                    var(--tilt) 0,
                    0 50%,
                    var(--tilt) 100%,
                    100% 100%,
                    100% 0
                    );
                clip-path: polygon(var(--tilt) 0, 0 50%, var(--tilt) 100%, 100% 100%, 100% 0);
                left: 100%;
                z-index: -1;
            }
            .btn-18:hover:before {
                transform: translateX(100%);
            }
            .btn-18:hover:after {
                transform: translateX(-100%);
            }
        </style>

    </head>
    <body>
        <!-- ***** Header Area Start ***** -->
        <jsp:include page="header1.jsp"/>
        <div class="main-image">
            <div class="heading_banner container">
                <h1><span>Syllabus</span></h1>
                <a class="button banner_button" href="#lists">scroll down to see website content &nbsp;<i class="fa fa-hand-point-down"></i></a>
            </div>
        </div>
        <!-- ***** Header Area End ***** -->

        <!--syllabus list section start-->
        <div id="lists" class="container" style="margin-top: 5rem;">
            <form method="post" action="SearchSyllabus">
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2" style="text-align: right">
                            <label class="form-check-label" for="inlineFormCheck">
                                Enter Syllabus: 
                            </label>
                        </td>
                        <td>
                            <div class="form-inline">
                                <div class="input-group">
                                    <input name="searchSyllabus?index=1"  type="text" id="txtKeyword" class="form-control rounded mr-2" style="width:50px;font-size: inherit;" />
                                    <span class="input-group-btn">
                                        <input type="submit"  id="btnSearch" class="btn btn-primary" style="width:100px;" />
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>

                </table>
            </form>
            <div class="table-users">
                <div class="header">Syllabus List</div>
                <table>
                    <thead>
                        <tr>
                            <th>Syllabus ID</th>
                            <th>Subject Code</th>
                            <th>Syllabus Name</th>
                            <th>isActive</th>
                            <th>isApproved</th>
                            <th>DecisionNo MM/dd/yyyy</th>
                            <c:if test="${sessionScope.roleName == 'admin' || 'crdd'}">
                                <th>Edit</th>
                            </c:if>
                        </tr>
                    </thead>
                    <tbody>
                    <div>
                        <c:forEach items="${list}" var="s">
                            <tr>
                                <td>${s.sID}</td>
                                <td>${s.subjectCode}</td>
                                <td><a href="SyllabusDetail?sid=${s.sID}" style="color: white;">${s.sName}</a></td>
                                <td>${s.isActive}</td>
                                <td>${s.isApproved}</td>
                                <td>${s.decisionNo}</td>
                                <c:if test="${sessionScope.roleName == 'admin' || 'crdd'}">
                                    <td class="d-flex">
                                        <a id="update" href="updateSyllabus?sid=${s.sID}" style="color:#fff"><i class="fa fa-pen-to-square fa-lg"></i></a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a id="delete" style="color:#fff" onclick="messageSuccess(${s.sID})"><i class="fa fa-trash fa-lg"></i></a>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                        <div class="paging" style="text-align: center; background-color: gold">
                            <c:forEach begin="1" end="${endPage}" var="i">
                                <a href="#" style="color: black; font-weight: bold;">${i}</a>
                            </c:forEach>
                        </div>
                    </div>
                    </tbody>
                </table>
            </div>
        </div>
        <c:if test="${sessionScope.roleName == 'admin' || 'crdd'}">
            <div class="mt-5" style="margin-left: 45%;">
                <a href="syllabusAdd.jsp">
                    <button class="btn-18">
                        <span class="text-container">
                            <span class="text">Create Syllabus</span>
                        </span>
                    </button>
                </a>
            </div>
        </c:if>

        <!--syllabus list section end-->

        <!--footer section start-->
        <jsp:include page="footer1.jsp"/>
        <!--footer section end-->

        <!--message edit data success-->
        <script>
            function messageSuccess(id) {
                var option = confirm('Are you sure to delete?');
                if (option === true) {
                    window.document.location.href = "deleteSyllabus?sid=" + id;
                }
            }
        </script>
    </body>
</html>
